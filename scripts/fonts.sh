#!/usr/bin/env bash

directory() {
  for font in FiraCode Mononoki; do
    fonts_dir="${HOME}/.fonts/${font}"
    if [ ! -d "${fonts_dir}" ]; then
      echo "mkdir -p $fonts_dir"
      mkdir -p "${fonts_dir}"
    else
      echo "Found fonts dir $fonts_dir"
    fi
  done
}

FiraCode() {
  local version=$(curl -sL https://github.com/tonsky/FiraCode/releases/latest | grep "Fira_Code" | grep -o "[0-9]*\.[0-9]*" | head -1)
  local file=Fira_Code_v${version}.zip

  curl --fail --location --show-error https://github.com/tonsky/FiraCode/releases/download/${version}/${file} --output ${file}
  unzip -o -q -d "${HOME}/.fonts/FiraCode" ${file}
  rm ${file}
}

Mononoki() {
  for type in Bold BoldItalic Italic Regular; do
    file_path="${HOME}/.fonts/Mononoki/Mononoki-${type}.ttf"
    file_url="https://github.com/madmalik/mononoki/blob/master/export/webfont/mononoki-${type}.ttf?raw=true"
    wget -O "${file_path}" "${file_url}"
  done
}

directory
FiraCode
Mononoki
