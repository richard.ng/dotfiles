containers() {
  prompt="Select option to remove Docker container(s):"
  PS3=$'\n'"$prompt"
  options=($(docker container ls -a | awk 'NR>1 {print $2}'))

  echo
  if [ ${#options[@]} -eq 0 ]; then
    echo "No containers running... exiting"
    exit
  fi
  docker container ls -a
  echo

  select opt in "one" "all" "exited" "quit"; do
    case $opt in
      one)
        docker container rm -f $(docker container ls -a | awk 'NR>1' | fzf | awk '{print $1}')
        break;;
      all)
        docker container rm -f $(docker container ls -aq)
        break;;
      exited)
        docker container rm -f $(docker container ls -aqf status=exited)
        break;;
      quit)
        exit;;
    esac
  done
}

images() {
  prompt="Select option to remove Docker image(s):"
  PS3=$'\n'"$prompt"
  options=($(docker image ls -a | awk 'NR>1 {print $1}'))

  echo
  if [ ${#options[@]} -eq 0 ]; then
    echo "No images available... exiting"
    exit
  fi
  docker image ls -a
  echo

  select opt in "one" "all" "dangling" "quit"; do
    case $opt in
      one)
        docker image rm -f $(docker image ls -a | awk 'NR>1' | fzf | awk '{print $3}')
        break;;
      all)
        read -p "Are you sure? " -n 1 -r
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]; then
          docker image rm -f $(docker image ls -aq)
        fi
        break;;
      dangling)
        docker image rm -f $(docker image ls -aqf dangling=true)
        break;;
      quit)
        exit;;
    esac
  done
}

prompt="Select option to manage Docker:"
PS3=$'\n'"$prompt"
select opt in "containers" "images"; do
  case $opt in
    containers)
      containers
      break;;
    images)
      images
      break;;
  esac
done
