getGitlabRepos() {
  GITLAB_REPOS=$(curl -s -H "Authorization: Bearer $GITLAB_PRIVATE_TOKEN" \
    "https://gitlab.com/api/v4/projects?membership=true&per_page=999" \
    | jq --raw-output --compact-output ".[]  | {"name":.path,"url":.ssh_url_to_repo}")
  echo $GITLAB_REPOS
}

getGithubRepos() {
  GITHUB_REPOS=$(curl -s -H "Accept: application/vnd.github.v3+json" \
    -H "Authorization: token $GITHUB_PRIVATE_TOKEN" \
    "https://api.github.com/user/repos" \
    | jq --raw-output --compact-output ".[] | {"name":.name,"url":.ssh_url}")
  echo $GITHUB_REPOS
}

promptVCS() {
  prompt="Select version control platform:"
  PS3=$'\n'"$prompt"

  select opt in "gitlab" "github" "quit"; do
    case $opt in
      gitlab)
        REPOS=$(getGitlabRepos)
        promptAmount $REPOS
        break;;
      github)
        REPOS=$(getGithubRepos)
        promptAmount $REPOS
        break;;
      quit)
        exit;;
    esac
  done
}

promptAmount() {
  prompt="Select number of repos to clone/pull:"
  PS3=$'\n'"$prompt"
  echo

  select opt in "one" "all" "quit"; do
    case $opt in
      one)
        local name=$(echo "$@" | jq -r ".name" | fzf)
        local url=$(echo "$@" | tr ' ' '\n' | grep "$name" | awk -F'"' '{print $8}')
        clonePull "$name" "$url"
        break;;
      all)
        for repo in "$@"; do
          local name=$(echo "$repo" | jq -r ".name")
          local url=$(echo "$repo" | jq -r ".url")

          clonePull "$name" "$url"
        done
        break;;
      quit)
        exit;;
    esac
  done
}

clonePull() {
  if [ ! -d "$1" ]; then
    echo -n "Cloning $1 ( $2 ) ... "
    git clone "$2" &
    wait
  else
    echo -n "$1 already exists, pulling instead ... "
    cd "$1" && git pull --no-rebase &
    wait
  fi
}

promptVCS
