OS=$(grep ^ID= /etc/os-release | sed 's/ID=//')

. "$DOTFILES/scripts/log.sh"

checkError() {
  if [ "$?" = 0 ]; then
    log_success "Finished"
  elif [ "$?" = 1 ]; then
    log_error "Exit code: $?"
    exit $?
  fi
}

arch() {
  packages
  fonts
  zsh
  vim
}

wsl() {
  update
  packages
  zsh
  vim
  go
  # run viminit
  # run :PlugInstall in vim
}

update() {
  for name in update upgrade autoremove; do
    log "apt $name"
    sudo apt $name
    checkError
  done
}

updateOS() {
  update
  log "Installing update-manager-core"
  sudo apt install update-manager-core
  checkError
  log "Installing new OS version"
  sudo do-release-upgrade
  checkError
}

packages() {
  log "Installing packages"
  if [ "$OS" = "ubuntu" ]; then
    for name in curl wget unzip git jq tmux htop xclip zsh silversearcher-ag shellcheck mpv ranger; do
      log "Installing $name"
      sudo apt install $name
      checkError
    done
  elif [ "$OS" = "arch" ]; then
    for name in curl wget unzip git jq tmux htop xclip zsh the_silver_searcher shellcheck mpv ranger; do
      log "Installing $name"
      sudo pacman -S $name
      checkError
    done
  fi
}

zsh() {
  log "Installing zsh"
  if [ "$OS" = "ubuntu" ]; then
    sudo apt install zsh
  elif [ "$OS" = "arch" ]; then
    sudo pacman -S zsh
  fi
  checkError

  log "Changing default shell to zsh"
  chsh -s "$(which zsh)"
  checkError

  log "Downloading ohmyzsh"
  sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" "" --unattended
  checkError

  log "Downloading zsh plugins"
  sudo git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  checkError
  sudo git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
  checkError

  log "Changing default theme"
  sed -i -e 's/ZSH_THEME=.*/ZSH_THEME="ys"/g' ~/.zshrc
  checkError

  log "Adding plugins to zshrc"
  sed -i -e 's/(git)/(git zsh-autosuggestions zsh-syntax-highlighting)/g' ~/.zshrc
  checkError

  log "Adding custom script to zshrc"
  printf "\nsource ~/dev/repos/dotfiles/.dev_env" >> ~/.zshrc
  checkError
}


vim() {
  current=$(pwd)
  cd "$DEV" >/dev/null || exit

  log "Cloning Vim repo"
  if [ ! -d "vim" ]; then
    git clone https://github.com/vim/vim.git
  else
    cd vim && git pull --no-rebase
  fi

  if [ "$OS" = "ubuntu" ]; then
    log "Installing required packages"
    sudo apt install libncurses-dev xorg-dev build-essential libpython3-dev
    checkError
  fi

  cd "$DEV/vim/src" >/dev/null || exit
  log "Running 'make'"
  sudo ./configure --enable-python3interp
  make
  checkError
  log "Running 'make install'"
  sudo make install
  checkError
  cd "$current" >/dev/null || exit

  # uninstall using:
  # sudo make uninstall
  # sudo make distclean
}

ctop() {
  version=$(curl -Ls https://github.com/bcicen/ctop/releases/latest | grep -o "[0-9]*\.[0-9]*\.[0-9]*" | head -1)

  log "Downloading ctop ${version}"
  sudo wget "https://github.com/bcicen/ctop/releases/download/v${version}/ctop-${version}-linux-amd64" -O /usr/local/bin/ctop
  checkError

  log "Changing permissions"
  sudo chmod +x /usr/local/bin/ctop
  checkError
}

vscode() {
  url=$(curl -s https://update.code.visualstudio.com/latest/linux-deb-x64/stable | grep -o "http.*")
  version=$(echo "$url" | grep -o "[0-9]*\.[0-9]*\.[0-9]*-[0-9]*")

  log "Downloading vscode ${version}"
  wget "${url}"
  checkError

  log "Installing vscode ${version}"
  sudo apt install "./code_${version}_amd64.deb"
  checkError

  log "Removing .deb install package"
  rm "code_${version}_amd64.deb"
  checkError
}

chrome() {
  log "Downloading latest chrome"
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  checkError

  log "Installing latest chrome"
  sudo apt install ./google-chrome-stable_current_amd64.deb
  checkError

  log "Removing .deb install package"
  rm google-chrome-stable_current_amd64.deb
  checkError
}

firefox() {
  log "Downloading latest Firefox"
  wget -O firefox-latest.tar.bz2 "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-CA"
  checkError

  log "Tarring Firefox"
  tar -xjf firefox-latest.tar.bz2
  checkError

  log "Moving Firefox to root"
  sudo mv firefox /opt/firefox
  checkError

  log "Symlinking Firefox"
  sudo ln -s /opt/firefox/firefox /usr/bin/firefox
  checkError

  log "Removing .tar.bz2 install package"
  rm firefox-latest.tar.bz2
  checkError
}

anaconda() {
  version=$(curl -s https://www.anaconda.com/distribution/\#linux | grep "Linux Installer" | grep -o "[0-9]*\.[0-9]*")

  log "Downloading anaconda ${version}"
  wget "https://repo.anaconda.com/archive/Anaconda3-${version}-Linux-x86_64.sh"
  checkError

  log "Installing anaconda ${version}"
  bash "Anaconda3-${version}-Linux-x86_64.sh"
  checkError

  log "Removing .sh install script"
  rm "Anaconda3-${version}-Linux-x86_64.sh"
  checkError
}

miniconda() {
  log "Downloading latest miniconda"
  wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
  checkError

  log "Installing latest miniconda"
  bash Miniconda3-latest-Linux-x86_64.sh
  checkError

  log "Removing .sh install script"
  rm Miniconda3-latest-Linux-x86_64.sh
  checkError
}

go() {
  version=$(curl -s https://go.dev/dl/ | grep "Stable versions" -a3 | grep -o "go.*[0-9]")

  log "Downloading go ${version}"
  wget "https://dl.google.com/go/${version}.linux-amd64.tar.gz"
  checkError

  log "Installing go ${version}"
  sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf "${version}.linux-amd64.tar.gz"
  checkError

  log "Removing .gz install package"
  rm "${version}.linux-amd64.tar.gz"
  checkError
}

fonts() {
  log "Running custom font install script"
  bash "$DOTFILES/scripts/fonts.sh"
  checkError

  # list monospace fonts using:
  # fc-list : family spacing | grep -e spacing=100 -e spacing=90
  # spacing must be 100 to use fonts
  # force fonts to be 100 spacing using custom font config
  log "Making .config directory"
  mkdir -p ~/.config
  checkError
  log "Copying custom font config"
  cp -r "$DOTFILES/.config/fontconfig" ~/.config
  checkError

  log "Rebuilding font cache"
  fc-cache -frv # force font cache rebuild
  checkError
}

kitty() {
  log "Installing kitty terminal"
  curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin \
    launch=n
  checkError

  # Create a symbolic link to add kitty to PATH
  sudo ln -sf ~/.local/kitty.app/bin/kitty /usr/bin/kitty
  # Place the kitty.desktop file somewhere it can be found by the OS
  cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
  # If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
  cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
  # Update the path to the kitty icon in the kitty.desktop file(s)
  sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop

  # list kitty font families using:
  # kitty list-fonts
  log "Making .config directory"
  mkdir -p ~/.config
  checkError
  log "Copying kitty config"
  cp -r "$DOTFILES/.config/kitty" ~/.config
  checkError
}

slack() {
  version=$(curl -s https://slack.com/release-notes/linux | grep -o "<h2>Slack\s[0-9]\{0,\}\.[0-9]\{0,\}\.[0-9]\{0,\}" | head -1 | sed 's/<h2>Slack //g')
  log "Downloading Slack ${version}"
  wget -O slack.deb "https://downloads.slack-edge.com/releases/linux/${version}/prod/x64/slack-desktop-${version}-amd64.deb"
  checkError
  log "Installing Slack"
  sudo apt install ./slack.deb
  checkError
  log "Removing .deb install package"
  rm slack.deb
  checkError
}

discord() {
  log "Downloading Discord"
  wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
  checkError
  log "Installing Discord"
  sudo apt install ./discord.deb
  checkError
  log "Removing .deb install package"
  rm discord.deb
  checkError
}

keybase() {
  log "Downloading keybase"
  curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
  checkError
  log "Installing keybase"
  sudo apt install ./keybase_amd64.deb
  checkError
  log "Removing .deb install package"
  rm keybase_amd64.deb
  checkError
  log "Running keybase"
  run_keybase
  checkError
}

minikube() {
  log "Downloading minikube"
  curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
  checkError
  log "Installing minikube"
  sudo dpkg -i minikube_latest_amd64.deb
  checkError
  log "Removing .deb install package"
  rm minikube_latest_amd64.deb
  checkError
}

kubectl() {
  log "Downloading kubectl"
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  checkError
  log "Installing kubectl"
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  checkError
  log "Removing kubectl binary"
  rm kubectl
  checkError
}

azurecli() {
  curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
}

rbenv(){
  log "Installing prerequisite packages"
  sudo apt install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev
  checkError

  log "Installing rbenv"
  git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
  checkError
  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  checkError
}

mpv() {
  current=$(pwd)
  cd "$DEV" >/dev/null || exit

  log "Installing dependencies"
  # may be an incomplete list
  sudo apt install autoconf libtool libfribidi-dev libharfbuzz-dev yasm libluajit-5.1-dev

  log "Cloning build repo"
  git clone https://github.com/mpv-player/mpv-build.git
  checkError

  log "Running ./rebuild script"
  cd mpv-build && ./rebuild -j4
  checkError
}

youtube() {
  current=$(pwd)
  cd "$DEV" >/dev/null || exit

  log "Cloning repos"
  git clone https://github.com/yt-dlp/yt-dlp
  checkError
  git clone https://github.com/pystardust/ytfzf
  checkError

  echo 'script-opts=ytdl_hook-ytdl_path=/home/richard/dev/yt-dlp/yt-dlp.sh' > ~/.config/mpv/mpv.conf
  echo 'ytdl-raw-options=sub-lang=en,write-auto-sub=' >> ~/.config/mpv/mpv.conf

  cd "$current" >/dev/null || exit
}

ranger() {
  log "Making .config directory"
  mkdir -p ~/.config
  checkError
  log "Copying ranger config"
  cp -r "$DOTFILES/.config/ranger" ~/.config
  checkError
}

rust() {
  log "Downloading and running install script"
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  checkError
  log "Downloading and installing rust-analyzer"
  curl -L https://github.com/rust-analyzer/rust-analyzer/releases/latest/download/rust-analyzer-x86_64-unknown-linux-gnu.gz |
    gunzip -c - > ~/.cargo/bin/rust-analyzer
  chmod +x ~/.cargo/bin/rust-analyzer
  checkError
}

ngrok() {
  log "Downloading install archive"
  wget -O ngrok-v3-stable-linux-amd64.tgz "https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.tgz"
  checkError
  log "Extracting ngrok"
  sudo tar xvzf ngrok-v3-stable-linux-amd64.tgz -C /usr/local/bin
  checkError
  log "Removing .tgz package"
  rm ngrok-v3-stable-linux-amd64.tgz
  checkError
}

docker() {
  log "Downloading convenience script"
  curl -fsSL https://get.docker.com -o get-docker.sh
  checkError
  log "Running convenience script"
  sudo sh get-docker.sh
  checkError
  log "Removing convenience script"
  rm get-docker.sh
  checkError
}

if [ -z "$1" ]; then
  func=$(grep "()" "$DOTFILES/scripts/install.sh" | tr -d "() {" | sed '$d' | fzf)
  bash "$DOTFILES/scripts/install.sh" "$func"
fi

for func in "$@"; do
  $func
done