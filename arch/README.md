# Arch Linux

## Intro
This folder contains scripts to fully install and configure a new installation of Arch Linux in a virtual machine (VM) with a windows host.

## Usage

1. Run `vbox.bat` on a windows host machine with virtual box installed.
 - will need to modify the paths to the VBoxManage utility, your VM folder, and the Arch Linux installation iso.
 - this will create and configure a VM for Arch Linux.

2. Start the new Arch VM.
 - the above step will have mounted the install media if it completed successfully.
 - after booting up, you should automatically be logged in as root: `root@archiso ~ #`

3. Run `curl https://gitlab.com/richard.ng/dotfiles/-/raw/master/arch/install.sh > install.sh` to download the install script.

4. Run `bash install.sh` and follow the prompts to install Arch Linux to the VM.
 - once complete, the VM will shutdown.
 - remove arch iso from the VM virtual drive before restarting.

5. Restart the VM, log in to user account and run `bash dev/post_install.sh`
 - this will install git, clone this repo, and run a script to install openbox and guest utils.

6. Run `startx` to start openbox.
 - the installation is now complete; you can now customize by installing any desired packages.

## My personal custom setup

1. Run the install script from this repo to install my customize arch setup: `bash scripts/install.sh arch`.

2. Run `viminit` to install a vim plugin manager and copy .vimrc to your home folder.
 - may need to restart terminal.

3. Open vim and run `:PlugInstall` to install plugins, most importantly FZF.
