#!/bin/bash
curl -s https://gitlab.com/richard.ng/dotfiles/-/raw/master/scripts/log.sh > log.sh
source log.sh
trap "rm log.sh" 0

confirm() {
  while true; do
    log "Continue? (y/n)"
    read -rsn1
    case $REPLY in 
      y|Y) return 0;;
      n|N) return 1;;
      *) echo;;
    esac
  done
}

askDisk() {
  log "Listing available block devices..."
  lsblk
  DISK="/dev/$(lsblk | grep disk | awk '{print $1}')"

  echo
  log "Input block device:"
  read DISK
  log "Will use input: $DISK"
  confirm
  if [ "$?" != 0 ]; then
    echo && askDisk
  fi
}

partition() {
  log "Listing available block devices..."
  lsblk
  DISK="/dev/$(lsblk | grep disk | awk '{print $1}')"

  echo
  log "Will use default: $DISK"
  confirm
  if [ "$?" != 0 ]; then
    echo && askDisk
  fi
  echo

  log "Partitioning..."
  log "Setting partition table to GPT"
  sgdisk -g $DISK

  log "Setting first partition: EFI boot partition, 500MB"
  sgdisk -n 0:0:+500M $DISK && sgdisk -t 1:EF00 $DISK

  log "Setting second partition: swap partition, 1000MB"
  sgdisk -n 0:0:+1000M $DISK && sgdisk -t 2:8200 $DISK

  log "Setting third partition: root/home partition, remaining disk space"
  sgdisk -n 0:0:0 $DISK && sgdisk -t 3:8300 $DISK

  log "Making file systems..."
  log "Setting FAT32 file system on EFI boot partition"
  PART1="${DISK}1"
  mkfs.fat -F32 $PART1

  log "Setting swap file system on swap partition"
  PART2="${DISK}2"
  mkswap $PART2
  log "Activating swap partition"
  swapon $PART2

  log "Setting EXT4 file system on root/home partition"
  PART3="${DISK}3"
  mkfs.ext4 $PART3
}

install() {
  log "Mounting partitions..."
  mount $PART3 /mnt
  mkdir -p /mnt/boot
  mount $PART1 /mnt/boot

  log "Installing packages..."
  pacstrap /mnt base base-devel linux linux-firmware networkmanager

  log "Generating fstab file..."
  genfstab -U /mnt >> /mnt/etc/fstab
}

enterChroot() {
  log "Copying script to chroot"
  cp install.sh /mnt/root
  chmod a+x /mnt/root/install.sh

  log "Entering chroot..."
  arch-chroot /mnt /root/install.sh chroot
}

configure() {
  log "Enabling network manager"
  systemctl enable NetworkManager.service

  log "Setting time zone"
  ln -sf /usr/share/zoneinfo/America/Vancouver /etc/localtime
  hwclock --systohc

  log "Setting locale"
  echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
  echo LANG=en_US.UTF-8 > /etc/locale.conf
  locale-gen
}

askHost() {
  log "Enter hostname:"
  read host
  log "Will use input: $host"
  confirm
  if [ "$?" != 0 ]; then
    echo && askHost
  fi
}

setHost() {
  askHost
  echo

  log "Adding host to hostname file"
  echo "$host" > /etc/hostname

  log "Adding host to hosts file"
  cat <<EOF >> /etc/hosts
  127.0.0.1   localhost
  ::1         localhost
  127.0.1.1   $host.localdomain   $host
EOF
}

rootPassword() {
  log "Enter new root password:"
  passwd
  if [ "$?" != 0 ]; then
    echo && rootPassword
  fi
}

askUser() {
  log "Enter new user name:"
  read user
  log "Will use input: $user"
  confirm
  if [ "$?" != 0 ]; then
    echo && askUser
  fi
}

userPassword() {
  log "Enter new password for user: $user"
  passwd $user
  if [ "$?" != 0 ]; then
    echo && userPassword
  fi
}

setupUser() {
  askUser
  echo

  log "Adding new user: $user"
  useradd -m $user
  echo
  userPassword

  log "Adding user to list of sudoers"
  sed -i "/root ALL/ a $user ALL=(ALL) ALL" /etc/sudoers

  log "Making directories: home/$user/dev/repos"
  mkdir -p /home/$user/dev/repos

  log "Downloading post_install script"
  curl https://gitlab.com/richard.ng/dotfiles/-/raw/master/arch/post_install.sh > post_install.sh

  log "Copying post_install script to home/$user/dev"
  cp post_install.sh /home/$user/dev

  log "Changing ownership of /home/$user/dev/ to $user"
  chmod a+x /home/$user/dev/post_install.sh
  chown -R $user: /home/$user/dev
}

bootloader() {
  log "Installing bootloader package"
  pacman -S grub efibootmgr

  log "Making bootloader directory"
  mkdir /boot/efi

  log "Mounting bootloader directory"
  mount /dev/sda1 /boot/efi

  log "Installing bootloader"
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB

  log "Generating configuration file"
  grub-mkconfig -o /boot/grub/grub.cfg
}

if [ -z "$1" ]; then
  partition
  install
  enterChroot

  log "Unmounting partitions"
  umount -R /mnt

  log "Installation complete"
  log "System will now shutdown"
  log "Before restarting, remove Arch iso from virtual drive by going to VM settings > storage"

  log "Press any key to continue"
  read -rsn1

  shutdown now
elif [ "$1" == "chroot" ]; then
  configure
  setHost
  rootPassword
  setupUser
  bootloader
fi
