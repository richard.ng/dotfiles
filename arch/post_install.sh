#!/bin/bash
curl -s https://gitlab.com/richard.ng/dotfiles/-/raw/master/scripts/log.sh > log.sh
source log.sh
trap "rm log.sh" 0

log "Intalling Git"
sudo pacman -S git

log "Cloning dotfiles repo"
cd ~/dev/repos
git clone https://gitlab.com/richard.ng/dotfiles

log "Installing Openbox"
sudo pacman -S openbox xorg-xinit xorg-server xorg-fonts-misc

log "Copying default xinitrc config to home folder"
cp /etc/X11/xinit/xinitrc ~/.xinitrc

log "Copying default openbox configs"
mkdir -p ~/.config/openbox
cp /etc/xdg/openbox/* ~/.config/openbox/

bash ~/dev/repos/dotfiles/scripts/install.sh kitty

log "Adding kitty to autostart"
echo kitty >> ~/.config/openbox/autostart

log "Installing virtual box guest utils"
sudo pacman -S virtualbox-guest-utils

log "Enabling vboxservice"
sudo systemctl enable vboxservice.service

log "Modifying xinitrc"
head -n -5 ~/.xinitrc >> tmp && mv tmp ~/.xinitrc
echo VBoxClient-all >> ~/.xinitrc
echo exec openbox-session >> ~/.xinitrc
