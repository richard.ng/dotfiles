@echo off

set /p name="Enter VM name: "

set vboxmanage="D:\Programs\Oracle\VirtualBox\VBoxManage"
set vboxvm="D:\VirtualBox VMs\%name%\%name%.vdi"
set vboxiso="C:\Users\Richard\Downloads\archlinux-2020.11.01-x86_64.iso"

echo.
echo "Creating VM..."
timeout /t 1 > NUL
%vboxmanage% createvm --name "%name%" --ostype "ArchLinux_64" --register --default

echo.
echo "Modifying VM settings..."
timeout /t 1 > NUL
%vboxmanage% modifyvm "%name%" --memory 6144 --vram 128 --graphicscontroller vboxsvga --firmware efi

echo.
echo "Creating VM volume..."
timeout /t 1 > NUL
%vboxmanage% createmedium --filename %vboxvm% --size 10000

echo.
echo "Attaching VM volume..."
timeout /t 1 > NUL
%vboxmanage% storageattach "%name%" --storagectl SATA --port 0 --type hdd --medium %vboxvm%

echo.
echo "Attaching ISO..."
timeout /t 1 > NUL
%vboxmanage% storageattach "%name%" --storagectl IDE --port 0 --device 0 --type dvddrive --medium %vboxiso%

echo.
echo "Complete"
timeout /t 1 > NUL
