alias dev='cd $DEV'
alias repos='cd $REPOS'
alias cls='clear && printf "\e[3J"'
alias l='LC_COLLATE=C ls -ahl --hyperlink=auto'
alias dd='docker compose down'
alias du='docker compose up'
alias deact='conda deactivate'
alias dockerup='sudo /usr/sbin/service docker start'
alias dockerdown='sudo /usr/sbin/service docker stop'

cd() {
    builtin cd "$@" && LC_COLLATE=C ls -ahl --hyperlink=auto
}

tmuxa() {
  # find latest resurrect save
  savefile=$(find ~/.tmux/resurrect -type f -printf "%T@ %p\n" | sort -n | tail -1 | cut -f2- -d" ")
  session=$(head -1 "$savefile" | awk '{print $2}')
  tmux new -s "$session"
}

gitstat() {
  current=$(pwd)
  cd "$REPOS" >/dev/null || exit
  find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(
    cd {} && if [ -n "$(git status -s)" ]; then
      echo {} && git status -s;
    fi)' \;
  cd "$current" >/dev/null || exit
}

dock() {
  bash "$DOTFILES/scripts/docker.sh"
}

repo() {
  current=$(pwd)
  cd "$REPOS" >/dev/null || exit
  bash "$DOTFILES/scripts/git.sh"
  cd "$current" >/dev/null || exit
}

walls() {
    current=$(pwd)
    cd "$REPOS/wallpaper-scraper" >/dev/null || exit
    . "$CONDAPATH"
    conda activate python
    python3 walls.py
    cd "$current" >/dev/null || exit
}

act() {
    . "$CONDAPATH"
    conda activate "$1"
}

viminit() {
    cp -r "$DOTFILES/.vim" "$HOME"
    cp "$DOTFILES/.vimrc" "$HOME"
    cp "$DOTFILES/.ignore" "$HOME"
    curl -s -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    echo "Vim Initialized"
}

tmuxinit() {
    cp "$DOTFILES/.tmux.conf" "$HOME"
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm 2>/dev/null
    echo "Tmux Initialized"
}

sshkey() {
  if [ -z "$2" ]; then
    echo "Usage: sshkey <rsa/ed25519> <email>"
  else
    if [ "$1" = "rsa" ]; then
      KEY="rsa"
      ssh-keygen -t rsa -b 4096 -C "$1"
    elif [ "$1" = "ed25519" ]; then
      KEY="ed25519"
      ssh-keygen -t ed25519 -C "$1"
    fi
    if [ "$(uname)" = "Darwin" ]; then
      pbcopy < ~/.ssh/id_$KEY.pub
    elif [ "$(uname)" = "Linux" ]; then
      xclip -sel clip < ~/.ssh/id_$KEY.pub
    fi
    echo "Public SSH key copied to clipboard"
    echo "Please add SSH key to your profile now"
    echo "Test SSH key was added correctly using: 'ssh -T git@gitlab.com'"
  fi
}

gpgkey() {
  if [ -z "$1" ]; then
    echo "Usage: gpgkey <email>"
  else
    gpg --full-gen-key
    GPG=$(gpg --list-secret-keys --keyid-format LONG "$1" | grep "sec\s" | awk '{print $2}' | awk -F / '{print $2}')
    if [ "$(uname)" = "Darwin" ]; then
      gpg --armor --export "$GPG" | pbcopy
    elif [ "$(uname)" = "Linux" ]; then
      gpg --armor --export "$GPG" | xclip -sel clip
    fi
    echo "Public GPG key copied to clipboard"
    git config --global user.signingkey "$GPG"
    git config --global commit.gpgsign true
    echo "Please add GPG key to your profile now"
  fi
}

sshaws() {
  chmod 400 "$1"
  ssh -i "$1" ubuntu@ec2-54-200-231-212.us-west-2.compute.amazonaws.com
}

sshpub() {
  chmod 400 "$1"
  ssh-keygen -y -f "$1"
}

yt() {
  current=$(pwd)
  cd "$DEV/ytfzf" >/dev/null || exit
  . "$CONDAPATH"
  conda activate python
  if [ -n "$2" ]
  then
    ./ytfzf -mt "$1" "$2"
  else
    ./ytfzf -mt "$1"
  fi
  # ytfzf
  # requires pip install ueberzug for thumbnails

  # alternative search, no thumbnails
  # $DEV/yt-dlp/yt-dlp.sh -O "%(title)s -- %(webpage_url)s -- %(upload_date)s" ytsearch5:""
  # list formats
  # $DEV/yt-dlp/yt-dlp.sh -F $(webpage_url)
  # play video
  # mpv $(webpage_url)
  # change formats with
  # mpv --ytdl-format=bestvideo[height<=480]+bestaudio $(webpage_url)
  cd "$current" >/dev/null || exit
}
