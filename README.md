# dotfiles

## New installs
`install.sh` contains scripts to set up a new Linux installation for development.

Run script with `bash install.sh <function>`

## Repo management

- You will need to create a personal access token for Gitlab and/or Github.
- Export `GITLAB_PRIVATE_TOKEN` and/or `GITHUB_PRIVATE_TOKEN` in your terminal