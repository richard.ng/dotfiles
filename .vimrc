" General settings
syntax on
colorscheme monokai

set cursorline
set cursorcolumn
set number
set hlsearch
set splitbelow
set splitright
set noswapfile
set nofixendofline
set autoread
set backspace=indent,eol,start
"set autochdir
set viminfo=%,<100,'100,!
"text wrapping
set linebreak
set wrap
set list
set listchars=eol:¬,tab:→\ ,trail:•,nbsp:⎵
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
let g:terminal_ansi_colors = [
\  '#272922',
\  '#f92672',
\  '#a6e22e',
\  '#f4bf75',
\  '#66d9ef',
\  '#ae81ff',
\  '#a1efe4',
\  '#f8f8f2',
\  '#75715e',
\  '#f92672',
\  '#a6e22e',
\  '#f4bf75',
\  '#66d9ef',
\  '#ae81ff',
\  '#a1efe4',
\  '#f9f8f5',
\]

" Filetype formats
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
filetype plugin indent on
augroup filemaps
  au FileType python setlocal tabstop=4 softtabstop=4 shiftwidth=4
  au FileType go setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
  au FileType go setlocal listchars+=tab:\ \ 
  au BufRead,BufNewFile *.dev_env set filetype=sh
  au BufRead,BufNewFile *.md set virtualedit+=all
augroup END

" Status line
let &t_ut=''
set noshowmode
set laststatus=2
set statusline=
set statusline+=%{ChangeStatuslineColor()}
set statusline+=%0*\ %{toupper(g:currentmode[mode()])}\ 
set statusline+=%2*\ %{getcwd()}\ 
set statusline+=%3*%{FugitiveHead()}
set statusline+=%9*\ %F
set statusline+=%3*\ buffer\ %n
set statusline+=%9*%m
set statusline+=%=
set statusline+=\ %p%%\ 
set statusline+=%2*\ %l:%c\ 
set statusline+=%9*\ %{strftime(\"%I:%M\ %p\")}\ 

hi User2 guifg=#9c64fe
hi User3 cterm=bold guifg=#57d1ea
hi User9 guifg=#fa8419

let g:currentmode={
    \ 'n'  : 'Normal',
    \ 'no' : 'Normal·Operator Pending',
    \ 'v'  : 'Visual',
    \ 'V'  : 'V·Line',
    \ "\<C-V>" : 'V·Block',
    \ 's'  : 'Select',
    \ 'S'  : 'S·Line',
    \ '^S' : 'S·Block',
    \ 'i'  : 'Insert',
    \ 'R'  : 'Replace',
    \ 'Rv' : 'V·Replace',
    \ 'c'  : 'Command',
    \ 'cv' : 'Vim Ex',
    \ 'ce' : 'Ex',
    \ 'r'  : 'Prompt',
    \ 'rm' : 'More',
    \ 'r?' : 'Confirm',
    \ '!'  : 'Shell',
    \ 't'  : 'Terminal'
    \}

function! ChangeStatuslineColor()
  if (mode() =~# '\v(n|no)')
    exe 'hi! StatusLine cterm=bold guibg=#afd700 guifg=#ff0000'
  elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
    exe 'hi! StatusLine cterm=bold guibg=#767676 guifg=#ff8700'
  elseif (mode() ==# 'i')
    exe 'hi! StatusLine cterm=bold guibg=#5fd7ff guifg=#ff0000'
  else
    exe 'hi! StatusLine cterm=bold guibg=#afd700 guifg=#ff0000'
  endif
  return ''
endfunction

" Commands
command! ClearRegisters for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor
command! ClearBuffers :%bd
command! ClearMarks :delmarks!|delmarks 1234567890
command! -bang -nargs=? -complete=dir Ag call fzf#vim#ag(<q-args>, '--path-to-ignore ~/.ignore --hidden', fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}), <bang>0)

" Keybinds
map <silent> <C-e> :NERDTreeToggle<CR>
map <silent> <C-t> :NERDTreeToggle %<CR>
map <C-f> :FZF<CR>
map <C-a> :Ag<CR>

map <C-w>% :vsp<CR>
map <C-w>" :sp<CR>
map <C-w>x :close<CR>
map <C-w>c :tabnew<CR>
map <C-w>n :tabn<CR>

nmap <C-p> <Plug>MarkdownPreviewToggle

nnoremap <Leader>gs :Git 
nnoremap <Leader>gd :Gvdiffsplit 
nnoremap <Leader>gf :diffget //2<CR>
nnoremap <Leader>gj :diffget //3<CR>
nnoremap <Leader>gl :Git log -- %<CR>

" Plugin settings
let NERDTreeShowHidden=1
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'

let g:markbar_num_lines_context = 3

let g:ale_linters = {
      \'python': ['pylint', 'flake8'],
      \'go': ['gofmt','golint','govet','gopls'],
      \'sh': ['shellcheck'],
      \'javascript': ['eslint'],
      \'typescript': ['eslint','tsserver'],
      \'rust': ['analyzer']
      \}
let g:ale_fixers = {
      \'go': ['goimports'],
      \'rust': ['rustfmt']
      \}
let g:ale_completion_enabled = 1

let g:vrc_response_default_content_type = 'application/json'
let g:vrc_auto_format_response_patterns = {
      \ 'json': 'jq'
      \}

let g:go_doc_popup_window = 1

let g:Hexokinase_highlighters = [ 'backgroundfull' ]

let g:tagalong_additional_filetypes = ['javascript']

let g:NERDCustomDelimiters = {
      \ 'javascriptreact': { 'left': '{/*', 'right': '*/}' },
      \ 'typescriptreact': { 'left': '{/*', 'right': '*/}' }
      \}

let g:mta_filetypes = {
      \ 'javascriptreact': 1,
      \ 'typescriptreact': 1
      \}

packadd! matchit
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" :PlugInstall
" :PlugClean

call plug#begin('~/.vim/plugs')
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'jeetsukumaran/vim-buffergator'
Plug 'rhysd/vim-syntax-christmas-tree'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdcommenter'
Plug 'junegunn/vim-peekaboo'
Plug 'yilin-yang/vim-markbar'
Plug 'dense-analysis/ale'
Plug 'diepm/vim-rest-console'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'gyim/vim-boxdraw'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'rust-lang/rust.vim'
Plug 'mattn/emmet-vim'
Plug 'AndrewRadev/tagalong.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'roxma/vim-tmux-clipboard'
Plug 'Valloric/MatchTagAlways'
call plug#end()